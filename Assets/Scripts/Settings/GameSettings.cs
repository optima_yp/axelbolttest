using Assets.Scripts.Character;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Settings
{
    [CreateAssetMenu(menuName = "Tools/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        [field: SerializeField]
        public MainCharacter MainCharacterPrefab { get; private set; }

        [field: SerializeField]
        public MovementSettingUI MovementSettingUi { get; private set; }

        [field: SerializeField] 
        public float GameRestartDelay { get; private set; } = 2f;
    }
}
