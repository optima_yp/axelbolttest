using System;
using System.Collections;
using System.Net.Mail;
using Assets.Scripts.BaseComponents;
using Assets.Scripts.Finishing;
using UnityEngine;

namespace Assets.Scripts.Restart
{
    public class RestartDelayAction : MonoBehaviour
    {
        [Min(0)]
        [SerializeField]
        private float delay = 2f;

        public event Action ActionAfterDelay;

        private void Awake()
        {
            MessageBus.Subscribe<CharacterFinishCommand>(CharacterFinished);
        }

        private void CharacterFinished(CharacterFinishCommand command)
        {
            throw new NotImplementedException();
        }

        private IEnumerator DoTaskAfterDelay()
        {
            yield return new WaitForSeconds(delay);
            ActionAfterDelay?.Invoke();
        }

        private void OnDestroy()
        {

        }
    }
}
