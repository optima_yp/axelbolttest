using Assets.Scripts.Character;
using Assets.Scripts.FieldBorder;
using Assets.Scripts.Settings;
using Assets.Scripts.UI;
using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Startup
{
    public class GameStartup : MonoBehaviour
    {
        [SerializeField]
        private Transform characterSpawnPoint;

        [SerializeField]
        private GameSettings gameSettings;

        [SerializeField]
        private Transform finish;

        [SerializeField]
        private Transform uiTransform;

        [SerializeField]
        private FieldBorderSettings fieldBorderSettings;

        private MainCharacter character;
        private MovementSettingUI movementSettingUI;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            InstantiateGameEnvironment();
            GameStaticData.GameRestartDelay = gameSettings.GameRestartDelay;
        }

        private void OnDrawGizmos()
        {
            DebugUtilities.DrawRect(fieldBorderSettings.Rectangle);
        }

        private void InstantiateGameEnvironment()
        {
            character =
                Instantiate(gameSettings.MainCharacterPrefab, characterSpawnPoint.position, Quaternion.identity);
            character.Initialize(fieldBorderSettings, finish.position, characterSpawnPoint.transform.position);
            movementSettingUI = Instantiate(gameSettings.MovementSettingUi, uiTransform);
        }
    }
}