using Assets.Scripts.Settings;
using UnityEngine;

namespace Assets.Scripts.Startup
{
    public abstract class GameStartInitializedObject : MonoBehaviour
    {
        public abstract void Initialize(GameSettings settings);
    }
}
