namespace Assets.Scripts.Character.MovingStrategy
{
    public interface IMovementStepLengthCalculation
    {
        float CalculateStepLength();    
    }
}
