using Assets.Scripts.Character.Movement;
using UnityEngine;

namespace Assets.Scripts.Character.MovingStrategy
{
    public class AcceleratedMovementStepLengthCalculation : IMovementStepLengthCalculation
    {
        private readonly float acceleration;
        private readonly MainCharacterMovementSystem mainCharacterMovementSystem;

        public AcceleratedMovementStepLengthCalculation(MainCharacterMovementSystem mainCharacterMovementSystem, float acceleration)
        {
            this.acceleration = acceleration;
            this.mainCharacterMovementSystem = mainCharacterMovementSystem;
        }

        public float CalculateStepLength()
        {
            var acceleratedSpeed = mainCharacterMovementSystem.MovementSpeed + acceleration * Time.deltaTime;
            return acceleratedSpeed * Time.deltaTime;
        }
    }
}
