using Assets.Scripts.Character.Movement;
using UnityEngine;

namespace Assets.Scripts.Character.MovingStrategy
{
    public class SpeedLimitedAcceleratedMovementStepLengthCalculation : IMovementStepLengthCalculation
    {
        private readonly float speedLimit;
        private readonly float acceleration;
        private readonly MainCharacterMovementSystem mainCharacterMovementSystem;

        public SpeedLimitedAcceleratedMovementStepLengthCalculation(float speedLimit, float acceleration, MainCharacterMovementSystem mainCharacterMovementSystem)
        {
            this.speedLimit = speedLimit;
            this.acceleration = acceleration;
            this.mainCharacterMovementSystem = mainCharacterMovementSystem;
        }

        public float CalculateStepLength()
        {
            var speed = Mathf.Clamp(mainCharacterMovementSystem.MovementSpeed + acceleration * Time.deltaTime, 0, speedLimit);
            return speed * Time.deltaTime;
        }
    }
}
