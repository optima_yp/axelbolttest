namespace Assets.Scripts.Character.Movement.Messages
{
    public readonly struct StartMoveCommand
    {
        public readonly MainCharacterMovementSettings Settings;

        public StartMoveCommand(MainCharacterMovementSettings settings)
        {
            this.Settings = settings;
        }
    }
}
