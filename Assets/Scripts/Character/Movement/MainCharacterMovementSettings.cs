namespace Assets.Scripts.Character.Movement
{
    public readonly struct MainCharacterMovementSettings
    {
        public readonly float Speed;
        public readonly float Acceleration;
        public readonly float TriggerArea;

        public MainCharacterMovementSettings(float speed, float acceleration, float triggerArea)
        {
            Speed = speed;
            Acceleration = acceleration;
            TriggerArea = triggerArea;
        }
    }
}
