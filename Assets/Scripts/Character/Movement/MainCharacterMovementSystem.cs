using System;
using Assets.Scripts.BaseComponents;
using Assets.Scripts.Character.Movement.Messages;
using Assets.Scripts.Character.MovingStrategy;
using Assets.Scripts.Character.TargetSetting;
using Assets.Scripts.FieldBorder;
using UnityEngine;

namespace Assets.Scripts.Character.Movement
{
    public class MainCharacterMovementSystem : ICharacterUpdateSystem
    {
        private readonly MainCharacter character;
        private readonly FieldBorderSystem fieldBorderSystem;
        private readonly Vector3 finishPosition;
        private readonly Camera mainCamera;
        private readonly ObjectsAggregator objectsAggregator;

        private MainCharacterMovementSettings movementSettings;
        private IMovementStepLengthCalculation currentCalculationLogic;
        private ICharacterTargetSetting currentTargetSetter;

        public MainCharacterMovementSystem(MainCharacter character,
            FieldBorderSystem fieldBorderSystem, Vector3 finishPosition)
        {
            this.character = character;
            this.fieldBorderSystem = fieldBorderSystem;
            this.finishPosition = finishPosition;
            mainCamera = Camera.main;
            objectsAggregator = new ObjectsAggregator();
            MessageBus.Subscribe<StopMovementCommand>(StopMove);
            MessageBus.Subscribe<StartMoveCommand>(StartMove);
            MessageBus.Subscribe<MainCharacterMovementSettings>(ApplyMovementSettings);
        }

        public float MovementSpeed { get; private set; }

        private void ApplyMovementSettings(MainCharacterMovementSettings settings)
        {
            movementSettings = settings;
            objectsAggregator.Clear();
            objectsAggregator.AddData(new AcceleratedMovementStepLengthCalculation(this, settings.Acceleration));
            objectsAggregator.AddData(new SpeedLimitedAcceleratedMovementStepLengthCalculation(settings.Speed, settings.Acceleration, this));
            objectsAggregator.AddData(new ClosestTargetRelativeToPointSetting(mainCamera, fieldBorderSystem));
            objectsAggregator.AddData(new FinishTargetSetting(finishPosition));
        }

        private void StartMove(StartMoveCommand command)
        {
            ApplyMovementSettings(command.Settings);
            currentCalculationLogic = objectsAggregator.GetData<SpeedLimitedAcceleratedMovementStepLengthCalculation>();
            currentTargetSetter = objectsAggregator.GetData<FinishTargetSetting>();
        }

        private void StopMove(StopMovementCommand command)
        {
            currentCalculationLogic = null;
            currentTargetSetter = null;
            MovementSpeed = 0;
        }

        public void Run()
        {
            if (currentCalculationLogic == null || currentTargetSetter == null) return;
            CheckMousePosition();
            var target = currentTargetSetter.SetTarget();
            target = fieldBorderSystem.ClampPoint(target);
            var step = currentCalculationLogic.CalculateStepLength();
            character.transform.position = Vector3.MoveTowards(character.transform.position, target, step);
            MovementSpeed = step / Time.deltaTime;
        }

        private void CheckMousePosition()
        {
            var mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            var distance = Vector2.Distance(character.transform.position, mousePosition);
            if (distance <= movementSettings.TriggerArea + character.GetColliderRadius())
            {
                currentCalculationLogic = objectsAggregator.GetData<AcceleratedMovementStepLengthCalculation>();
                currentTargetSetter = objectsAggregator.GetData<ClosestTargetRelativeToPointSetting>();
                character.CharacterEmotionView.SetSadEmotion();
            }
            else
            {
                currentCalculationLogic = objectsAggregator.GetData<SpeedLimitedAcceleratedMovementStepLengthCalculation>();
                currentTargetSetter = objectsAggregator.GetData<FinishTargetSetting>();
                character.CharacterEmotionView.SetDefaultEmotion();
            }
        }

        public void Dispose()
        {
            MessageBus.Unsubscribe<StopMovementCommand>(StopMove);
            MessageBus.Unsubscribe<StartMoveCommand>(StartMove);
            MessageBus.Unsubscribe<MainCharacterMovementSettings>(ApplyMovementSettings);
        }
    }
}
