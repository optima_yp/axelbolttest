using UnityEngine;

namespace Assets.Scripts.Character.TargetSetting
{
    public interface ICharacterTargetSetting
    {
        Vector3 SetTarget();
    }
}
