using UnityEngine;

namespace Assets.Scripts.Character.TargetSetting
{
    public class FinishTargetSetting : ICharacterTargetSetting
    {
        private readonly Vector3 finishPosition;

        public FinishTargetSetting(Vector3 finishPosition)
        {
            this.finishPosition = finishPosition;
        }

        public Vector3 SetTarget()
        {
            return finishPosition;
        }
    }
}
