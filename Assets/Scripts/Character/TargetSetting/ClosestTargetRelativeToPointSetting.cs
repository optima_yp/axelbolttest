using System;
using Assets.Scripts.FieldBorder;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Assets.Scripts.Character.TargetSetting
{
    public class ClosestTargetRelativeToPointSetting : ICharacterTargetSetting
    {
        private readonly float normalizeMultiplier = 1f;

        private readonly Camera mainCamera;

        private Vector3 previousMousePosition;

        public ClosestTargetRelativeToPointSetting(Camera mainCamera, FieldBorderSystem fieldBorder)
        {
            this.mainCamera = mainCamera;
            previousMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            var rect = fieldBorder.GetBorder();
            normalizeMultiplier = Convert.ToSingle(Math.Sqrt(Math.Pow(rect.width, 2) + Math.Pow(rect.height, 2))) * 2;
        }

        public Vector3 SetTarget()
        {
            var mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            var objectsDelta = mousePosition - previousMousePosition;
            var direction = objectsDelta.normalized * normalizeMultiplier;
            if (!previousMousePosition.Equals(mousePosition))
                previousMousePosition = mousePosition;
            return direction;
        }
    }
}
