using System.Collections.Generic;
using Assets.Scripts.BaseComponents;
using Assets.Scripts.Character.EmotionVisualization;
using Assets.Scripts.Character.Movement;
using Assets.Scripts.FieldBorder;
using Assets.Scripts.Finishing;
using UnityEngine;

namespace Assets.Scripts.Character
{
    [RequireComponent(typeof(CircleCollider2D))]
    [RequireComponent(typeof(MainCharacterEmotionView))]
    public class MainCharacter : MonoBehaviour
    {
        private CircleCollider2D collider;
        private Vector3 startPosition;
        private List<ICharacterUpdateSystem> characterUpdateSystems;

        public MainCharacterEmotionView CharacterEmotionView { get; private set; }

        public void Initialize(FieldBorderSettings fieldBorderSettings, Vector3 finishPosition, Vector3 startPosition)
        {
            CharacterEmotionView = GetComponent<MainCharacterEmotionView>();
            collider = GetComponent<CircleCollider2D>();
            characterUpdateSystems = new List<ICharacterUpdateSystem>();
            var fieldBorderSystem = new FieldBorderSystem(fieldBorderSettings.Rectangle, GetColliderRadius());
            characterUpdateSystems.Add(new MainCharacterMovementSystem(this, fieldBorderSystem, finishPosition));
            MessageBus.Subscribe<CharacterFinishCommand>(MoveCharacterToSpawn);
            this.startPosition = startPosition;
        }

        public float GetColliderRadius() => collider.radius * transform.localScale.x;

        private void MoveCharacterToSpawn(CharacterFinishCommand command)
        {
            transform.position = startPosition;
        }

        private void Update()
        {
            characterUpdateSystems.ForEach(x => x.Run());
        }

        private void OnDestroy()
        {
            characterUpdateSystems.ForEach(x => x.Dispose());
            MessageBus.Unsubscribe<CharacterFinishCommand>(MoveCharacterToSpawn);
        }
    }
}