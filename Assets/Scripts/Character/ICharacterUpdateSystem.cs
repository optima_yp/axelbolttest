using System;

namespace Assets.Scripts.Character
{
    public interface ICharacterUpdateSystem : IDisposable
    {
        void Run();
    }
}
