using UnityEngine;

namespace Assets.Scripts.Character.EmotionVisualization
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class MainCharacterEmotionView : MonoBehaviour
    {
        [SerializeField]
        private Sprite sadEmotion;
        [SerializeField]
        private Sprite relaxingEmotion;

        private Sprite defaultEmotion;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            defaultEmotion = spriteRenderer.sprite;
        }

        public void SetDefaultEmotion()
        {
            spriteRenderer.sprite = defaultEmotion;
        }

        public void SetRelaxingEmotion()
        {
            spriteRenderer.sprite = relaxingEmotion;
        }

        public void SetSadEmotion()
        {
            spriteRenderer.sprite = sadEmotion;
        }
    }
}
