using System.Collections;
using Assets.Scripts.BaseComponents;
using Assets.Scripts.Character;
using Assets.Scripts.Startup;
using UnityEngine;

namespace Assets.Scripts.Finishing
{
    [RequireComponent(typeof(Trigger2DEvents))]
    public class FinishDetector : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem particles;
        [SerializeField]
        private GameStartup gameStartup;

        private Trigger2DEvents trigger2DEvents;

        private void Awake()
        {
            particles.Stop();
            trigger2DEvents = GetComponent<Trigger2DEvents>();
            trigger2DEvents.OnTriggerEnter += CharacterFinishCross;
        }

        private void CharacterFinishCross(Collider2D collider)
        {
            var character = collider.GetComponent<MainCharacter>();
            if (!character) return;
            MessageBus.Publish(new CharacterFinishCommand());
            StartCoroutine(FinishEffectVisualize(character));
        }

        private IEnumerator FinishEffectVisualize(MainCharacter character)
        {
            particles.Play();
            character.CharacterEmotionView.SetRelaxingEmotion();
            yield return new WaitForSeconds(GameStaticData.GameRestartDelay);
            particles.Stop();
            character.CharacterEmotionView.SetDefaultEmotion();
        }

        private void OnDestroy()
        {
            trigger2DEvents.OnTriggerEnter -= CharacterFinishCross;
        }
    }
}