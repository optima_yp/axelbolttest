using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Assets.Scripts.FieldBorder
{
    public class FieldBorderSystem
    {
        private readonly Rect border;
        private readonly float characterRadius;

        private float borderMinX;
        private float borderMaxX;
        private float borderMinY;
        private float borderMaxY;

        public FieldBorderSystem(Rect rect, float characterRadius)
        {
            border = SubtractCharacterDiameterFromBorder(rect);
            this.characterRadius = characterRadius;
            InitializeBordersExtremum();
        }

        private Rect SubtractCharacterDiameterFromBorder(Rect rect) =>
            new Rect(rect.x, rect.y, rect.width - characterRadius, rect.height - characterRadius);

        private void InitializeBordersExtremum()
        {
            borderMinX = border.x - border.width / 2;
            borderMaxX = border.x + border.width / 2;
            borderMinY = border.y - border.height / 2;
            borderMaxY = border.y + border.height / 2;
        }

        public Rect GetBorder() => border;

        public Vector3 ClampPoint(Vector3 position)
        {
            var x = Mathf.Clamp(position.x, borderMinX, borderMaxX);
            var y = Mathf.Clamp(position.y, borderMinY, borderMaxY);
            return new Vector3(x, y, position.z);
        }
    }
}
