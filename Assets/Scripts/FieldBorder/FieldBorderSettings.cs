using System;
using UnityEngine;

namespace Assets.Scripts.FieldBorder
{
    [Serializable]
    public class FieldBorderSettings
    {
        [SerializeField]
        private float width = 2;
        [SerializeField]
        private float height = 2;
        [SerializeField]
        private Vector2 position = Vector2.zero;

        public Rect Rectangle => new Rect(position.x, position.y, width, height);
    }
}
