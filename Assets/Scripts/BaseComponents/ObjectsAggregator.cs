using System;
using System.Collections.Generic;

namespace Assets.Scripts.BaseComponents
{
    public class ObjectsAggregator
    {
        private Dictionary<Type, object> dataCollection;

        public ObjectsAggregator()
        {
            dataCollection = new Dictionary<Type, object>();
        }

        public void AddData<T>(T data)
        {
            if (!dataCollection.ContainsKey(typeof(T)))
            {
                dataCollection.Add(typeof(T), data);
            }
        }

        public T GetData<T>()
        {
            if (!dataCollection.ContainsKey(typeof(T)))
                return default;
            return (T)dataCollection[typeof(T)];
        }

        public void Clear() => dataCollection = new Dictionary<Type, object>();
    }
}
