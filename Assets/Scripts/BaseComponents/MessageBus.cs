using System;

namespace Assets.Scripts.BaseComponents
{
    public static class MessageBus
    {
        private static EventAggregator eventAggregator = new EventAggregator();

        public static void Subscribe<T>(Action<T> subscriber)
        {
            eventAggregator.AddListener(subscriber);
        }

        public static void Publish<T>(T message)
        {
            eventAggregator.Invoke(message);
        }

        public static void Unsubscribe<T>(Action<T> subscriber)
        {
            eventAggregator.RemoveListener(subscriber);
        }

        public static void Clear()
        {
            eventAggregator = new EventAggregator();
        }
    }
}
