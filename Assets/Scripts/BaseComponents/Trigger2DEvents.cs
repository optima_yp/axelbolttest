using System;
using UnityEngine;

namespace Assets.Scripts.BaseComponents
{
    public class Trigger2DEvents : MonoBehaviour
    {
        public event Action<Collider2D> OnTriggerEnter;
        public event Action<Collider2D> OnTriggerExit;
        public event Action<Collider2D> OnTriggerStay;

        private void OnTriggerEnter2D(Collider2D inputCollider)
        {
            OnTriggerEnter?.Invoke(inputCollider);
        }

        private void OnTriggerExit2D(Collider2D inputCollider)
        {
            OnTriggerEnter?.Invoke(inputCollider);
        }

        private void OnTriggerStay2D(Collider2D inputCollider)
        {
            OnTriggerEnter?.Invoke(inputCollider);
        }
    }
}
