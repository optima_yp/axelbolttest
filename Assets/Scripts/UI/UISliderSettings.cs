using System;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [Serializable]
    public class UISliderSettings
    {
        [field: SerializeField]
        public float MaximumPossibleSpeed { get; private set; }

        [field: SerializeField]
        public float MaximumPossibleTriggerArea { get; private set; }

        [field: SerializeField]
        public float MaximumPossibleAcceleration { get; private set; }
    }
}
