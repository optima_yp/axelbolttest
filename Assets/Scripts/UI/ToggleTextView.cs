using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleTextView : MonoBehaviour
    {
        [SerializeField]
        private Text text;
        [SerializeField]
        private string enablingText;

        private string defaultText;
        private Toggle toggle;

        private void Awake()
        {
            toggle = GetComponent<Toggle>();
            defaultText = text.text;
            toggle.onValueChanged.AddListener(SwitchText);
        }

        private void SwitchText(bool isOn)
        {
            if (isOn)
            {
                text.text = enablingText;
            }
            else
            {
                text.text = defaultText;
            }
        }

        private void OnDestroy()
        {
            toggle.onValueChanged.RemoveListener(SwitchText);
        }
    }
}
