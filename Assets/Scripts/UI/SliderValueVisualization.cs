using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(Slider))]
    public class SliderValueVisualization : MonoBehaviour
    {
        private const string TextDivider = ": ";

        [SerializeField]
        private Text text;

        private Slider slider;
        private string defaultText;

        private void Awake()
        {
            defaultText = text.text;
            slider = GetComponent<Slider>();
            UpdateTextValue(slider.value);
            slider.onValueChanged.AddListener(UpdateTextValue);
        }

        private void UpdateTextValue(float value)
        {
            text.text = $"{defaultText}{TextDivider}{value:0.0}";
        }

        private void OnDestroy()
        {
            slider.onValueChanged.RemoveListener(UpdateTextValue);
        }
    }
}
