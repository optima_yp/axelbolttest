using System.Collections;
using Assets.Scripts.BaseComponents;
using Assets.Scripts.Character.Movement;
using Assets.Scripts.Character.Movement.Messages;
using Assets.Scripts.Finishing;
using Assets.Scripts.Startup;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(MovementParametersUI))]
    public class MovementSettingUI : MonoBehaviour
    {
        [SerializeField]
        private Toggle startToggle;

        private MovementParametersUI movementParametersUI;

        private void Awake()
        {
            movementParametersUI = GetComponent<MovementParametersUI>();
            movementParametersUI.OnSettingsEdited += SendMovementSettings;
            startToggle.onValueChanged.AddListener(SendCommandToCharacter);
            MessageBus.Subscribe<CharacterFinishCommand>(BlockStartButtonAfterFinish);
        }

        private void BlockStartButtonAfterFinish(CharacterFinishCommand command)
        {
            startToggle.isOn = false;
            startToggle.interactable = false;
            StartCoroutine(EnableToggleAfterDelay());
        }

        private void SendMovementSettings(MainCharacterMovementSettings settings)
        {
            MessageBus.Publish(settings);
        }

        private void SendCommandToCharacter(bool isOn)
        {
            if (isOn)
            {
                MessageBus.Publish(new StartMoveCommand(movementParametersUI.GetSliderValues()));
            }
            else
            {
                MessageBus.Publish(new StopMovementCommand());
            }

        }

        private IEnumerator EnableToggleAfterDelay()
        {
            yield return new WaitForSeconds(GameStaticData.GameRestartDelay);
            startToggle.interactable = true;
        }

        private void OnDestroy()
        {
            movementParametersUI.OnSettingsEdited -= SendMovementSettings;
            startToggle.onValueChanged.RemoveListener(SendCommandToCharacter);
            MessageBus.Unsubscribe<CharacterFinishCommand>(BlockStartButtonAfterFinish);
        }
    }
}
