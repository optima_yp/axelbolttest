using System;
using Assets.Scripts.Character.Movement;
using Assets.Scripts.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class MovementParametersUI : MonoBehaviour
    {
        private const float SliderMinimumValue = 0f;

        public event Action<MainCharacterMovementSettings> OnSettingsEdited;

        [SerializeField] 
        private UISliderSettings settings;

        [SerializeField]
        private Slider speedSlider;

        [SerializeField]
        private Slider accelerationSlider;

        [SerializeField]
        private Slider triggerAreaSlider;

        private void Awake()
        {
            speedSlider.onValueChanged.AddListener(SliderValueChange);
            accelerationSlider.onValueChanged.AddListener(SliderValueChange);
            triggerAreaSlider.onValueChanged.AddListener(SliderValueChange);

            UIUtilities.SetSliderRange(speedSlider, settings.MaximumPossibleSpeed, SliderMinimumValue);
            UIUtilities.SetSliderRange(accelerationSlider, settings.MaximumPossibleAcceleration, SliderMinimumValue);
            UIUtilities.SetSliderRange(triggerAreaSlider, settings.MaximumPossibleTriggerArea, SliderMinimumValue);
        }

        private void SliderValueChange(float value) => OnSettingsEdited?.Invoke(GetSliderValues());
        
        public MainCharacterMovementSettings GetSliderValues()
        {
            return new MainCharacterMovementSettings(speedSlider.value, accelerationSlider.value,
                triggerAreaSlider.value);
        }

        private void OnDestroy()
        {
            speedSlider.onValueChanged.RemoveListener(SliderValueChange);
            accelerationSlider.onValueChanged.RemoveListener(SliderValueChange);
            triggerAreaSlider.onValueChanged.RemoveListener(SliderValueChange);
        }
    }
}
