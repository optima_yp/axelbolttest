using UnityEngine.UI;

namespace Assets.Scripts.Utilities
{
    public static class UIUtilities
    {
        public static void SetSliderRange(Slider slider, float maxValue, float minimumValue)
        {
            slider.minValue = minimumValue;
            slider.maxValue = maxValue;
        }
    }
}
