using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public static class DebugUtilities
    {
        public static void DrawRect(Rect rect)
        {
            Debug.DrawLine(new Vector3(rect.x - rect.width / 2, rect.y - rect.height / 2), new Vector3(rect.x + rect.width / 2, rect.y - rect.height / 2), Color.green);
            Debug.DrawLine(new Vector3(rect.x - rect.width / 2, rect.y - rect.height / 2), new Vector3(rect.x - rect.width / 2, rect.y + rect.height / 2), Color.red);
            Debug.DrawLine(new Vector3(rect.x + rect.width / 2, rect.y - rect.height / 2), new Vector3(rect.x + rect.width / 2, rect.y + rect.height / 2), Color.green);
            Debug.DrawLine(new Vector3(rect.x + rect.width / 2, rect.y + rect.height / 2), new Vector3(rect.x - rect.width / 2, rect.y + rect.height / 2), Color.red);
        }
    }
}
